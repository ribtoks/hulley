# How to setup the development environment

* `cd $GOROOT/src/`
* `git clone git@github.com:Ribtoks/hulley.git` - you need to have the environment in your **WORKSPACE**
* now you can build separate packages e.g. `cd cmd/cruck && go build`