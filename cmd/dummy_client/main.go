package main

import (
  "bytes"
  "io/ioutil"
  "net/http"
  "encoding/json"
  "math/rand"
  "time"
  "flag"
  // local
  "github.com/ribtoks/hulley/common"
  // remote
  "go.uber.org/zap"
)

var (
  eventsCountFlag = flag.Int("n", 10, "Number of events to send")
)

func main() {
  flag.Parse()

  common.InitLog([]string { "stdout" })
  
  events := &common.EventSlice{
    Events: make([]common.Event, *eventsCountFlag),
  }

  eventsBuffer, _ := json.Marshal(events)

  url := "http://localhost:8080/postevents"
  sentCount := 0
  
  for sentCount < *eventsCountFlag {
    randomTime := rand.Intn(800) + 1
    time.Sleep(time.Duration(randomTime) * time.Millisecond)
    
    request, err := http.NewRequest("POST", url, bytes.NewBuffer(eventsBuffer))
    request.Header.Set("Content-Type", "application/json")

    client := &http.Client{}
    response, err := client.Do(request)
    if err != nil {
      panic(err)
    }
    
    defer response.Body.Close()

    body, _ := ioutil.ReadAll(response.Body)

    common.Log.Info("response:",
      zap.String("status", response.Status),
      zap.ByteString("body", body))

    sentCount++
  }

  common.Log.Info("Done")
}
