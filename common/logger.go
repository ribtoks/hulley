package common

import (
  "go.uber.org/zap"
)

var Log *zap.Logger;

func InitLog(logFilepaths []string) error {  
  config := zap.NewProductionConfig()
  
  config.OutputPaths = logFilepaths
  
  zapLog, err := config.Build()
  if err != nil {
    return err
  }

  Log = zapLog
  
  defer Log.Sync()

  Log.Info("Logging initialized to " + logFilepaths[0])

  return nil
}
